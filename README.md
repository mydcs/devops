# Status

[gitlab-pipeline-image]: https://gitlab.com/mydcs/devops/badges/main/pipeline.svg
[gitlab-pipeline-link]: https://gitlab.com/mydcs/devops/-/commits/main
[gitlab-release-image]: https://gitlab.com/mydcs/devops/-/badges/release.svg
[gitlab-release-link]: https://gitlab.com/mydcs/devops/-/releases
[gitlab-stars-image]: https://img.shields.io/gitlab/stars/mydcs/devops?gitlab_url=https%3A%2F%2Fgitlab.com
[gitlab-stars-link]: https://hub.docker.com/r/mydcs/devops

[![gitlab-pipeline-image]][gitlab-pipeline-link] 
[![gitlab-release-image]][gitlab-release-link]
[![gitlab-stars-image]][gitlab-stars-link]

# How To's

## version.sh

Das Script version.sh dient dazu das aktuelle Repo zu taggen mit der nächsten Versionsnummer und ein CHANGELOG zu erstellen.

```bash
version.sh semver-component
```

Als semver-component stehen folgende Optionen zur Verfügung:

| Parameter   | Beschreibung          | Beispiel |
| ---         | ---                   | ---      |
| major       | Nächste Major Release | 0.8.3 -> 1.0.0, 1.4.3-alpha.4 -> 2.0.0 |
| minor       | Nächste Minor Release | 0.8.3 -> 0.9.0, 1.4.3-alpha.4 -> 1.5.0 |
| patch       | Nächste Patch Release | 0.8.3 -> 0.8.4, 1.4.3-alpha.4 -> 1.4.4 |
| build       | Nächstes Build        | 0.8.3 -> 0.8.3.1, 1.4.3-alpha.4 -> 1.4.3-alpha.5 |
| *, z.B. DEV | Individuelles Release | 0.8.3 -> 0.8.3-DEV.0, 1.4.3-alpha.4 -> 1.4.3-DEV.0 | 

Es werden alle übrigen Bestandteile der Version auf 0 gesetzt, oder entfernt.
