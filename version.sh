#!/bin/sh

version=$(git tag --sort=v:refname| tail -n1 | sed -E 's/^([0-9]*)\.([0-9]*)\.([0-9]*)(-[A-Za-z]*)?.?([0-9]*)?/\1 \2 \3 \4 \5/g')

export major=$(echo $version | awk '{print $1}')
export minor=$(echo $version | awk '{print $2}')
export patch=$(echo $version | awk '{print $3}')
export release=$(echo $version | awk '{print $4}')
export build=$(echo $version | awk '{print $5}')

case $1 in
	major )
		major=$((major + 1))
		minor=0
		patch=0
		release=""
		build=""
		;;
	minor )
		minor=$((minor + 1))
		patch=0
		release=""
		build=""
		;;
	patch )
		patch=$((patch + 1))
		release=""
		build=""
		;;
	build )
		release="${release:-}."
		build=$((build + 1))
		;;
	* )
		release="-$1."
		build="0"
		;;
esac

export newversion=${major:-0}.${minor:-0}.${patch:-0}${release:-}${build:-}
git tag -am "chore: tagging $newversion" $newversion
docker run -v "$PWD":/workdir quay.io/git-chglog/git-chglog:0.15.1 -o CHANGELOG.md
git add CHANGELOG.md
git commit -m "chore: CHANGELOG $newversion"
git push --follow-tags
