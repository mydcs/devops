<a name="unreleased"></a>
## [Unreleased]


<a name="0.0.3"></a>
## [0.0.3] - 2023-02-17
### Chore
- CHANGELOG 0.0.2

### Fix
- missing numbers for first release


<a name="0.0.2"></a>
## [0.0.2] - 2023-01-29
### Chore
- CHANGELOG

### Cicd
- Added CHANGELOG config
- Added release pipeline

### Doc
- Added README.md

### Fix
- not possible to increase more than two-digit numbers


<a name="0.0.1"></a>
## 0.0.1 - 2023-01-29
### Feat
- Added pure shell script for versioning and CHANGELOG creation


[Unreleased]: https://gitlab.com/mydcs/devops/compare/0.0.3...HEAD
[0.0.3]: https://gitlab.com/mydcs/devops/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/mydcs/devops/compare/0.0.1...0.0.2
